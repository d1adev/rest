var gulp = require('gulp');
var gulp_rename = require('gulp-rename');
var gulp_uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var del = require('del');
var runSequence = require('run-sequence');
var exec = require('child_process').exec;
var productionPath = 'prod/';
var nodemon = require('gulp-nodemon');
var watch = require('gulp-watch');

gulp.task('default', function (cb) {
    runSequence(['lint', 'clean'], /*'minify', Don't need that while debugging*/ 'start', 'modifs', cb);
});

gulp.task('lint', function () {
    return gulp.src('*.js')
        .pipe(jshint())
        .pipe(jshint.reporter());
});

gulp.task('clean', function () {
    return del(['prod/*'], function (err, paths){
        console.log('Deleted : \n', paths.join('\n'));
    });
});

gulp.task('minify', function () {
    return gulp.src('src/*.js')
        .pipe(gulp_uglify())
        .pipe(gulp_rename({ suffix: '.min' }))
        .pipe(gulp.dest('prod'));
});

gulp.task('start', function () {
    console.log('starting mongoDB');
    exec('c: && mongod', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
    });
    console.log('starting app.js');
    nodemon({
        script: 'prod/app.min.js'
    });
});

gulp.task('modifs', function () {
    gulp.src('src/*.js')
        .pipe(watch('src/*.js'))
        //.pipe(gulp_uglify()) TO ENABLE WHEN NOT DEBUGGING
        .pipe(gulp_rename({ suffix: '.min' }))
        .pipe(gulp.dest('prod'));
});