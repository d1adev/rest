var mongoose = require('mongoose');
var express = require('express');
var morgan = require('morgan');
var bodyparser = require('body-parser');
var Schema = mongoose.Schema;

var DataSchema = Schema({
    title: String,
    text: String,
    author: String
});

var Data = mongoose.model('Data', DataSchema);
/*
var test = new Data({ title: 'lol', text: 'lololol', author: 'lel' });
test.save(function (err, test) {
    if (err) {
        return console.error(err);
    }
})*/

console.log('Trying to connect to mongoDB');
mongoose.connect('mongodb://localhost/rest');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (cb) {
    startServer();
    console.log('connected');
});

// main function.

function startServer() {
    var app = express();
    //app.use(morgan('combined'));
    app.use(bodyparser.urlencoded());
    app.use(bodyparser.json());

    app.get('/title/:title', function (req, res) {
        getFromTitle(req.params.title, function (err, resp) {
            if (err !== null) {
                res.writeHead(200, {
                    "Content-Type": "application/json"
                });
                res.end(JSON.stringify({
                    success: false,
                    message: "not found"
                }));
            } else {
                console.log('no error');
                if (resp != null) {
                    res.writeHead(200, {
                        "Content-Type": "application/json"
                    });
                    res.end(JSON.stringify(resp));
                } else {
                    res.writeHead(404, {
                        "Content-Type": "application/json"
                    });
                    res.end(JSON.stringify({
                        success: false,
                        message: "not found"
                    }));
                    console.log(res);
                }
            }
        });
    });

    app.get('/text/:text', function (req, res) {
        getFromText(req.params.text, function (err, resp) {
            if (err !== null) {
                res.writeHead(200, {
                    "Content-Type": "application/json"
                });
                res.end(JSON.stringify({
                    success: false,
                    message: "not foud"
                }));
            } else {
                console.log('no error');
                if (resp != null) {
                    res.writeHead(200, {
                        "Content-Type": "application/json"
                    });
                    res.end(JSON.stringify(resp));
                } else {
                    res.writeHead(404, {
                        "Content-Type": "application/json"
                    });
                    res.end(JSON.stringify({
                        success: false,
                        message: "not found"
                    }));
                    console.log(res);
                }
            }
        });
    });

    app.get('/author/:author', function (req, res) {
        getFromAuthor(req.params.author, function (err, resp) {
            if (err !== null) {
                res.writeHead(200, {
                    "Content-Type": "application/json"
                });
                res.end(JSON.stringify({
                    success: false,
                    message: "not found"
                }));
            } else {
                console.log('no error');
                if (resp != null) {
                    res.writeHead(200, {
                        "Content-Type": "application/json"
                    });
                    res.end(JSON.stringify(resp));
                } else {
                    res.writeHead(404, {
                        "Content-Type": "application/json"
                    });
                    res.end(JSON.stringify({
                        success: false,
                        message: "not found"
                    }));
                    console.log(res);
                }
            }
        });
    });

    app.post('/message', function (req, res) {
        console.log(req.body.title + ' ' + req.body.text + ' ' + req.body.author);
        createNewPost({
            title: req.body.title,
            text: req.body.text,
            author: req.body.author
        }, function (err) {
            if (err === false) {
                res.writeHead(500, {
                    "Content-Type": "application/json"
                });
                res.end(JSON.stringify({
                    success: false,
                    message: "error while saving into the DB"
                }));
            } else {
                res.writeHead(201, {
                    "Content-Type": "application/json"
                });
                res.end(JSON.stringify({
                    success: true
                }));
            }
        });
    });


    app.put('/message/:id', function (req, res) {
        getFromId(req.params.id, function (err, data) {
            if (err){
                console.log('Error');
            } else {
                var post = data;
                var newPostData = new Data({
                    title: req.body.title,
                    text: req.body.text,
                    author: req.body.author
                });
                // TODO : Get the PUT data

                updatePost(post, newPostData, function(err, data){
                    if(err){
                        res.writeHead(500, {
                            "Content-Type": "application/json"
                        });
                        res.end(JSON.stringify({
                            success: false,
                            message: "error while updating"
                        }));
                    }
                    else{
                        res.writeHead(201, {
                            "Content-Type": "application/json"
                        });
                        res.end(JSON.stringify({
                            success: true
                        }));
                    }
                });
            }
        });
    });

    var server = app.listen(3000, function () {
        var host = server.address().address;
        var port = server.address().port;
        console.log('Running on ' + host + ':' + port);
    });
}

function getFromId(id, cb) {
    Data.findOne({
        '_id': id
    }, function (err, data) {
        if (err) {
            cb(true, null);
        } else {
            console.log(data);
            cb(null, data);
        }
    });
}


// TODO : Code this function.
function updatePost(post, newPost, cb){
    var _post = post;
    if(typeof newPost.title != 'undefined' && newPost.title != post.title){
        _post.title = newPost.title;
    }
    if(typeof newPost.text != 'undefined' && newPost.text != post.text){
        _post.text = newPost.text;
    }
    if(typeof newPost.author != 'undefined' && newPost.author != post.author){
        _post.author = newPost.author;
    }
    _post._id = post._id;

    Data.findOneAndUpdate({
        '_id': post._id
    }, _post, function(err, data){
        if(err){
            console.log(err);
            console.log(data);
            cb(true, null);
        }
        else{
            cb(null, data);
        }
    })
}

function getFromTitle(title, cb) {
    Data.findOne({
        'title': title
    }, function (err, data) {
        if (err) {
            cb(true, null);
        } else {
            console.log(data);
            cb(null, data);
        }
    });
}

function getFromText(text, cb) {
    Data.findOne({
        'text': text
    }, function (err, data) {
        if (err) {
            cb(true, null);
        } else {
            console.log(data);
            cb(null, data);
        }
    });
}

function getFromAuthor(author, cb) {
    Data.findOne({
        'author': author
    }, function (err, data) {
        if (err) {
            cb(true, null);
        } else {
            console.log(data);
            cb(null, data);
        }
    });
}

function createNewPost(data, cb) {
    var msg = new Data({
        title: data.title,
        text: data.text,
        author: data.author
    });
    msg.save(function (err, dt) {
        if (err) {
            cb(false);
        } else {
            cb(true);
        }
    });
}
